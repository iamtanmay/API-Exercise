﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using RESTAPI.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MySql.Data.MySqlClient;

namespace RESTAPI
{
    public class Startup
    {
        private readonly IConfiguration _config;

        private readonly string _connectionString;

        public Startup(IConfiguration config)
        {
            _config = config;
            _connectionString = $@"Server={_config["MYSQL_SERVER_NAME"]}; Database={_config["MYSQL_DATABASE"]}; Uid={_config["MYSQL_USER"]}; Pwd={_config["MYSQL_PASSWORD"]}";
			Console.WriteLine("Connectionstring : {0}", _connectionString);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            WaitForDBInit(_connectionString);
            services.AddDbContext<PassengerContext>(ops => ops.UseMySql(_connectionString));
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, PassengerContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            context.Database.Migrate();
            app.UseMvc();
        }

        private static void WaitForDBInit(string connectionString)
        {
            var connection = new MySqlConnection(connectionString);
            int retries = 1;
            while (retries < 11)
            {
                try
                {
                    Console.WriteLine("Connecting to db: {0}", retries);
                    connection.Open();
                    connection.Close();
                    break;
                }
                catch (MySqlException)
                {
                    Thread.Sleep(retries * 1000);
                    retries++;
                }
            }
        }
    }
}