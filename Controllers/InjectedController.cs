using RESTAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace RESTAPI.Controllers
{
    public class InjectedController: ControllerBase
    {
        protected readonly PassengerContext db;

        public InjectedController(PassengerContext context)
        {
            db = context;
        }
        
    }
}