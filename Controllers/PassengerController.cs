using System.Threading.Tasks;
using RESTAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace RESTAPI.Controllers
{
    [Route("/api/[controller]")]
    public class PassengerController: InjectedController
    {
        public PassengerController(PassengerContext context) : base(context) { }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetPassenger(int id)
        {
            var passenger = await db.Passengers.FindAsync(id);
            if (passenger == default(Passenger))
            {
                return NotFound();
            }
            return Ok(passenger);
        }

        [HttpGet]
        public async Task<IActionResult> GetPassengers()
        {
            return Ok(db.Passengers);
        }

        [HttpPost]
        public async Task<IActionResult> AddPassenger([FromBody] Passenger passenger)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            await db.AddAsync(passenger);
            await db.SaveChangesAsync();
            return Ok(passenger.ID);
        }

    }
}