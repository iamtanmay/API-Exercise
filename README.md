About:

Simple REST API built in dotnetcore 2.1 to fill/read MySQL 5.7 database. Works with docker-compose, but due to 
limitations in the IIS logging, I was unable to debug the dotnet containers in Kubernetes.

As an alternative, for the Kubernetes deployment - Xmysql (https://github.com/o1lab/xmysql) was used to generate the REST API.

TODO:

- Automatic Service IP retrieval using InitContainer + Kubernetes API. Currently External IPs for services are exported manually to secret
- Health check + Monitoring
- Initial seeding of MySQL is done with ConfigMap. This is manual. It would be nice to have SQL dump import and export automated.
- IIS Dotnet container works fine with docker-compose, but not in Kubernetes. This could be debugged. Dotnet can offer extended functionality.

Prerequisites:

- To run the REST API on Kubernetes - a kubernetes cluster with sufficient memory. Tested with gke cluster with 12 GB Memory and 3 vCPUs.

- docker for container building. Dockercompose for running linked containers. 

- cURL for making HTTP requests.

Kubernetes instructions:

- git clone this repository

- cd API-Exercise/k8/

- Create mysql secret (Caution with text formatting below)

    cat <<EOF | kubectl create -f -
    
    apiVersion: v1
    
    kind: Secret
    
    metadata:
    
    name: mysql
        
    type: Transparent
    
    stringData:
    
    MYSQL_USER: root
    
    MYSQL_PASSWORD: test
    
    MYSQL_DATABASE: Titanic
        
    EOF

- Create configmap for initial Database seed

    kubectl create configmap seed --from-file=Titanic.sql

- Deploy MySQL

    kubectl create -f mysql-service.yaml;kubectl create -f mysql-volumeclaim.yaml;kubectl create -f mysql.yaml;

- Wait for external IP of mysql

- Obtain IP for mysql service

    IFS=':' read -r -a array <<<$(kubectl describe service mysql | grep "Ingress:")
    export MYSQLIP=${array[1]}
    
- create Secret for Xmysql (Caution with text formatting below)

    cat <<EOF | kubectl create -f -
    apiVersion: v1
    kind: Secret
    metadata:
      name: xmysqlsecret
    type: Transparent
    stringData:
      DATABASE_HOST: $MYSQLIP
      DATABASE_USER: root
      DATABASE_PASSWORD: test
      DATABASE_NAME: Titanic
    EOF
    
- deploy Xmysql

    kubectl create -f xmysql.yaml;kubectl expose deployment xmysql --type="LoadBalancer";

- Wait for external IP of xmysql

- Get IP for Xmysql

    IFS=':' read -r -a array2 <<<$(kubectl describe service xmysql | grep "Ingress:")
    export XMYSQLIP=${array2[1]}

- Get URLs for REST requests

    export POSTURL=$XMYSQLIP
    
    POSTURL+="/api/Passengers"
    
    export GETURL=$XMYSQLIP
    
    GETURL+="/api/Passengers/"

#Kubernetes API Calls (Replace 35.241.195.162 with External IP of xmysql service):
# Add a passenger  
    curl -X POST \
	$POSTURL \
	-H 'content-type: application/json' -d \
	'{ "Survived": 1, "Pclass": 3, "Name": "TestUser1", "Sex": "male", "Age": 22, "Siblingsspouse": 1, "Parentschildren": 0, "Fare": 7.25 }'

# Add a passenger
    curl -X POST \
	$POSTURL \
	-H 'content-type: application/json' -d \
    '{ "Survived": 0, "Pclass": 2, "Name": "TestUser2", "Sex": "female", "Age": 45, "Siblingsspouse": 0, "Parentschildren": 2, "Fare": 15.25 }'

# Get passenger 888
    curl -X GET $GETURL+888

# List of other APIs (PUT, DELETE, exists, count, describe)
    curl -X GET $XMYSQLIP



Docker compose instructions:

- git clone this repository

cd API-Exercise

docker-compose -f ./Docker/docker-compose.yml build --no-cache

docker-compose -f ./Docker/docker-compose.yml up

#docker-compose API calls:

# Add a passenger  
    curl -X POST http://localhost:5000/api/Passenger/ -H 'cache-control: no-cache' -H 'content-type: application/json' -H 'postman-token: f63a259d-5c0a-baf5-3a56-583d672672a6' -d '{ "Survived": 1, "Pclass": 3, "Name": "TestUser1", "Sex": "male", "Age": 22, "SiblingsSpousesAboard": 1, "ParentsChildrenAboard": 0, "Fare": 725 }'

# Add a passenger  
    curl -X POST http://localhost:5000/api/Passenger/ -H 'cache-control: no-cache' -H 'content-type: application/json' -H 'postman-token: f63a259d-5c0a-baf5-3a56-583d672672a6' -d '{ "Survived": 0, "Pclass": 1, "Name": "TestUser2", "Sex": "Female", "Age": 22, "SiblingsSpousesAboard": 1, "ParentsChildrenAboard": 0, "Fare": 425 }'

# Get a passenger  
    curl -X GET http://localhost:5000/api/Passenger/888 -H 'cache-control: no-cache' -H 'content-type: application/json' -H 'postman-token: 9402340e-d28b-8ae7-4265-8d2ce3ca33aa'

# Get all passengers
    curl -X GET http://localhost:5000/api/Passenger/ -H 'cache-control: no-cache' -H 'content-type: application/json' -H 'postman-token: 9402340e-d28b-8ae7-4265-8d2ce3ca33aa'
  
#Notes