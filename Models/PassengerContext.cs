using Microsoft.EntityFrameworkCore;

namespace RESTAPI.Models
{
    public class PassengerContext: DbContext
    {
        public DbSet<Passenger> Passengers { get; set; }

        public PassengerContext(DbContextOptions options): base(options) { }
    }
}