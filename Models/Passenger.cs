using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RESTAPI.Models
{
    public class Passenger
    {
        [Required]
        public int ID { get; set; }
        
        [Required]
        public int Survived { get; set; }

        [Required]
        public int PClass { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(6)]
        public string Sex { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
		public int Siblingsspouse { get; set; }

        [Required]
		public int Parentschildren { get; set; }

        [Required]
        public double Fare { get; set; }
    }
}
